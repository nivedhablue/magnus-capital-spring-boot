(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <!-- Page Heading -->\n  <div class=\"row\">\n      <div class=\"col-xl-12\">\n          <h2 class=\"page-header\">\n              Magnus Capital Trading Platform\n          </h2>\n      </div>\n  </div>\n  <div>\n\n      <hr>\n      <!-- <alert *ngFor=\"let alert of alerts; let i = index\" [type]=\"alert.type\" dismissible=\"true\" (close)=\"closeAlert(i)\">\n          {{ alert?.msg }}\n      </alert>\n\n      <div>\n          <div class=\"card card-block\">\n            <div class=\"table-responsive\">\n              <table class=\"table table-dark\">\n                <thead>\n                  <tr>\n                    <th>#</th>\n                    <th>Ticker</th>\n                    <th></th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr class=\"clickable-row\">\n                    <th scope=\"row\">1</th>\n                    <td>AAPL</td>\n                    <td>\n                      <input class=\"form-check-input\" type=\"checkbox\" value=\"\" id=\"defaultCheck1\">\n                    </td>\n                  </tr>\n                  <tr class=\"clickable-row\">\n                    <th scope=\"row\">2</th>\n                    <td>TSLA</td>\n                    <td>\n                          <input class=\"form-check-input\" type=\"checkbox\" value=\"\" id=\"defaultCheck1\">\n                        </td>\n                  </tr>\n                </tbody>\n              </table>\n            </div>\n          </div>\n        </div> -->\n\n        <!-- <div>\n              {{stocks.name}}\n        </div> -->\n          \n        \n        <br>\n        \n        <form #f=\"ngForm\" name=\"ngForm\">\n          <div class=\"form-group col-md-6 mb-3\">\n                <label>Select Strategy</label>\n                <select name=\"selectStrategy\" class=\"form-control\" [(ngModel)]=\"strategyOption\" (change)=\"strategySelect()\">\n                  <option id=\"twomaOption\" value=\"twomaOption\">Two Moving Averages</option>\n                  <option id=\"bbOption\" value=\"bbOption\">Bollinger Bands</option>\n                </select>\n          </div>\n          <div class=\"form-group col-md-6 mb-3\">\n            <label>Enter ticker</label>\n            <input id=\"tickerInput\" type=\"text\" class=\"form-control\" placeholder=\"Enter ticker\" [(ngModel)]=\"tickerInput\" name=\"tickerInput\">\n          </div>\n\n          <div class=\"form-group col-md-6 mb-3\">\n            <label>Enter number of shares</label>\n            <input id=\"numShares\" type=\"text\" class=\"form-control\" placeholder=\"Enter number of shares\" [(ngModel)]=\"numShares\" name=\"numSharesInput\">\n        </div>\n\n          <div class=\"form-group col-md-6 mb-3\" >\n              <label>Enter long period</label>\n              <input id=\"lpInput\" type=\"text\" class=\"form-control\" placeholder=\"Enter Long Period\" [(ngModel)]=\"longPeriod\" [disabled]=\"!isTwoMA\" name=\"longPeriod\">\n          </div>\n\n            <div class=\"form-group col-md-6 mb-3\">\n                <label>Enter short period</label>\n                <input id=\"spInput\" type=\"text\" class=\"form-control\" placeholder=\"Enter Short Period\" [(ngModel)]=\"shortPeriod\" [disabled]=\"!isTwoMA\" name=\"shortPeriod\">\n          </div>\n\n           <div class=\"form-group col-md-6 mb-3\">\n                <label>Enter Period</label>\n                <input id=\"periodInput\" type=\"text\" class=\"form-control\" placeholder=\"Period\" [disabled]=\"isTwoMA\" [(ngModel)]=\"period\" name=\"first\">\n            </div>  \n        \n    </form>\n    <br>\n    <div align=\"right\">\n        <button name=\"start\" type=\"submit\" class=\"btn btn-success\" id=\"start\" (click)=\"onSubmit()\">Start</button> &nbsp;\n        <button name=\"stop\" type=\"submit\" class=\"btn btn-danger\" id=\"stop\">Stop</button>\n    </div>\n        <br>\n        <button name=\"stop\" type=\"submit\" class=\"btn btn-primary\" id=\"stop\" (click)=\"getTrades()\">Get History</button>\n\n        \n        <table class=\"table\">\n          <thead class=\"thead-inverse\">\n          <tr>\n              <th class=\"text-center\" style=\"width: 80px;\">ticker</th>\n              <th class=\"text-center\" style=\"width: 80px;\">Shares</th>\n              <th class=\"text-center\" style=\"width: 80px;\">Price</th>\n              <th class=\"text-center\" style=\"width: 80px;\">Action</th>\n              <th class=\"text-center\" style=\"width: 80px;\">ID</th>\n              <!-- <th class=\"text-center\">Body</th> -->\n          </tr>\n          </thead>\n          <tbody>\n          <tr *ngFor=\"let post of _tradesArray\">\n              <td class=\"text-center\" style=\"width: 80px;\">{{post.ticker}}</td>\n              <td class=\"text-center\" style=\"width: 80px;\">{{post.numShares}}</td>\n              <td class=\"text-center\" style=\"width: 80px;\">{{post.price}}</td>\n              <td class=\"text-center\" style=\"width: 80px;\">{{post.action}}</td>\n              <td class=\"text-center\" style=\"width: 80px;\">{{post.id}}</td>\n          </tr>\n          </tbody>\n      </table>\n   </div>      \n  </div>\n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _trade_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./trade.service */ "./src/app/trade.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(tradeService) {
        this.tradeService = tradeService;
        this.title = 'Magnus Capital Equity Trading Platform';
        // this.stock = this.homeService;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('TEST STRING FROM ANGULAR');
        this.tradeService.getAll().subscribe(function (data) {
            _this.stocks = data;
            console.log(data);
        });
    };
    // getHistory () {
    //   this.tradeService.getAllTrades().subscribe(data => {
    //     this.tradeList = data;
    //     console.log(this.tradeList);
    //   });
    // }
    AppComponent.prototype.getTrades = function () {
        var _this = this;
        this.tradeService.getAllTrades()
            .subscribe(function (resultArray) { return _this._tradesArray = resultArray; }, function (error) { return console.log('Error :: ' + error); });
    };
    AppComponent.prototype.strategySelect = function () {
        if (this.strategyOption === 'bbOption') {
            this.isTwoMA = false;
        }
        else {
            this.isTwoMA = true;
        }
    };
    AppComponent.prototype.onSubmit = function () {
        var _this = this;
        this.body = this.tickerInput + '_' + this.numShares + '_' + this.longPeriod + '_' + this.shortPeriod + '_' + this.period;
        console.log('this.body: ' + this.body);
        if (this.strategyOption === 'bbOption') {
            console.log('select BB');
            this.tradeService.runBB(this.body).subscribe(function (data) {
                _this.response = data.status;
                console.log('success!');
            });
        }
        else {
            console.log('select MA');
            this.tradeService.runTwoMA(this.body).subscribe(function (data) {
                _this.response = data.status;
                console.log('success!');
            });
        }
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_trade_service__WEBPACK_IMPORTED_MODULE_1__["TradeService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/trade.service.ts":
/*!**********************************!*\
  !*** ./src/app/trade.service.ts ***!
  \**********************************/
/*! exports provided: TradeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TradeService", function() { return TradeService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TradeService = /** @class */ (function () {
    function TradeService(http) {
        this.http = http;
    }
    TradeService.prototype.getAll = function () {
        return this.http.get('//localhost:9080/dashboard/home');
    };
    TradeService.prototype.getAllTrades = function () {
        return this.http.get('//localhost:9080/trades');
    };
    TradeService.prototype.getTrades = function () {
        return this.http
            .get('//localhost:9080/trades')
            .map(function (response) {
            return response.json();
        });
    };
    TradeService.prototype.runTwoMA = function (body) {
        // var body = "firstname=" + user.firstname + "&lastname=" + user.lastname + "&name=" + user
        console.log('In runTwoMA service');
        return this.http.post('//localhost:9080/runTWOMA', body);
    };
    // runTwoMA(): Observable<any> {
    //   // var body = "firstname=" + user.firstname + "&lastname=" + user.lastname + "&name=" + use
    //   console.log('In runTwoMA service');
    //   console.log('params.toString: ' + this.params.toString());
    //   return this.http.get('//localhost:9080/runTwoMA?param=params');
    // }
    TradeService.prototype.runBB = function (body) {
        return this.http.post('//localhost:9080/runBB', body);
    };
    TradeService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], TradeService);
    return TradeService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Administrator\Documents\magnus-capital-final\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map
package com.example.demo;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import jms.MessageHandler;
import magnuscapital.StockInfo;

public class Logging {
	private static Logger LOGGER = null;
	private static Logging singleInstance = null;
	private FileHandler fh;
	
	private Logging() {
		LOGGER = Logger.getAnonymousLogger();
		try {
			fh = new FileHandler("Logging.log");
		} catch (SecurityException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		LOGGER.setUseParentHandlers(false);
		LOGGER.addHandler(fh);
		SimpleFormatter formatter = new SimpleFormatter();
		fh.setFormatter(formatter);
		LOGGER.info("logging instantiated");
	}
	
	public static Logging getInstance() {
		if (singleInstance == null) {
			singleInstance = new Logging();
		}
		return singleInstance;
	}
	
	public Logger getLogger() {
		return LOGGER;
	}

}
